import { defineStore } from 'pinia'
export const useThemeStore = defineStore('theme', {
    state: () => ({
        theme: 'light', // Set a default theme
        dark: false,
    }),
    actions: {
        initializeTheme() {
            if (process.client) {
                const storedTheme = localStorage.getItem('theme');
                if (storedTheme) {
                    this.setTheme(storedTheme);
                } else {
                    this.setTheme('system'); // Default to system theme on first load
                }
            }
        },
        setTheme(theme) {
            if (theme === 'system') {
                const prefersDarkMode = window.matchMedia('(prefers-color-scheme: dark)').matches;
                this.theme = prefersDarkMode ? 'dark' : 'light';
            } else {
                this.theme = theme;
            }
            this.dark = this.theme === 'dark';
            document.documentElement.setAttribute('data-theme', this.theme);
            if (process.client) {
                localStorage.setItem('theme', this.theme);
            }
        },
    },
});
