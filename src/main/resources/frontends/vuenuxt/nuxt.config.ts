// @ts-ignore
import fs from 'fs';
// @ts-ignore
import path from 'path';
// @ts-ignore
import dotenv from 'dotenv';

interface ServerConfig {
  domainName: string;
  walletFile: string;
  blockchain: string;
  trustedFullNode: string;
  frontendDevBaseUrl: string;
  frontendBaseAPIUrl: string;
  fundAddress: number;
  fundAddressCooldown: number;
}

interface ClientConfig {
  location: string;
  framework: string;
  routeprefix: string;
}

interface AppConfig {
  server: ServerConfig;
  client: ClientConfig;
  activeClients: string[];
}

// Adjust the path to move up to the root directory
const configPath = path.resolve(__dirname, '../../../../../app.cfg');
let envConfig: Partial<AppConfig> = {};

try {
  // Attempt to read and parse app.cfg
  const configData = fs.readFileSync(configPath, 'utf-8');
  envConfig = JSON.parse(configData);
  console.log('Loaded configuration from app.cfg:', envConfig);
} catch (err) {
  console.warn('Failed to load app.cfg, falling back to .env');
  // Fallback to .env
  dotenv.config();
}

// Ensure env variables are set
process.env.BASE_URL = envConfig.server?.frontendDevBaseUrl || process.env.FRONTEND_DEV_BASE_URL
process.env.API_BASE_URL = envConfig.server?.frontendBaseAPIUrl || process.env.FRONTEND_BASE_API_URL
process.env.DOMAIN_NAME = envConfig.server?.domainName || process.env.DOMAIN_NAME

export default {
  app: {
    baseURL: envConfig.client?.vuenuxt?.routeprefix || '/'
  },
  ssr: false, // Disable server-side rendering to enable client-side rendering (CSR)
  css: [
    '~/static/css/main.css'
  ],

  site: {
    url: process.env.BASE_URL,
    name: 'Nexa',
    description: '',
    defaultLocale: 'en'
  },

  target: 'static',

  generate: {
    dir: 'public', // Output directory for the generated site
  },
  dir: {
    public: 'static', // Use 'static' folder for static files
  },

  url: [
    process.env.BASE_URL
  ],

  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {}
    }
  },

  image: {
    quality: 80,
    screens: {
      'xs': 320,
      'sm': 640,
      'md': 768,
      'lg': 1024,
      'xl': 1280,
      'xxl': 1536,
      '2xl': 1536
    },
    format: ['webp', 'avif', 'jpeg', 'png']
  },

  ogImage: {
    enabled: false
  },

  // The order of plugins is important
  plugins: [
    '~/plugins/directives.js',
    '~/plugins/session.js',
    '~/plugins/websocket.client.js',
    '~/plugins/wallet.client.js',
  ],

  components: [
    {
      path: '@/components',
      pathPrefix: false
    }
  ],

  runtimeConfig: {
    public: {
      domainName: process.env.DOMAIN_NAME,
      baseURL: process.env.BASE_URL,
      apiBaseUrl: process.env.API_BASE_URL,
      routePrefix: process.env.ROUTE_PREFIX,
      blockchain: envConfig.server?.blockchain || process.env.BLOCKCHAIN,
      enableFundAddress: (envConfig.server?.fundAddress ?? 0) > 0,
      fundAddressCooldown: envConfig.server?.fundAddressCooldown
    }
  },

  router: {
    middleware: ['fetchSession', 'checkWallet'],
  },

  // security: {
  //   headers: {
  //   },
  //   allowedHosts: [
  //     domainNameNoPort,
  //     domainName,
  //     'localhost:3000'
  //   ],
  //   contentSecurityPolicy: {
  //     'upgrade-insecure-requests': false,
  //     directives: {
  //       'default-src': ["'self'"],
  //       'script-src': ["'self'", domainName, domainNameNoPort, 'localhost:3000'],
  //       'style-src': ["'self'", "'unsafe-inline'"],
  //       'img-src': ["'self'", 'data:', domainName, domainNameNoPort, 'localhost:3000'],
  //       'connect-src': ["'self'", `ws://${domainName}`, domainName, domainNameNoPort, 'http://localhost:3000']
  //     }
  //   }
  // },
  modules: [
    "@nuxtjs/seo",
    "@nuxt/image",
    "@nuxt/content",
    "@nuxt/fonts",
    // "nuxt-security",
    "@nuxt/ui",
  ],
  compatibilityDate: '2024-07-06'
};
