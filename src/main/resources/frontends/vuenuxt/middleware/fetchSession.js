import { ref } from 'vue'
import { useNuxtApp } from '#app'

// Initialize reactive references
const serverFqdn = ref('')
const sessionId = ref('')
const session = ref({})
const qrURI = ref('')

// Define the function to fetch session data
const fetchSession = async () => {
    if (!session.value.sessionId) {
        try {
            const { $fetch } = useNuxtApp()
            const data = await $fetch(`/session`)
            serverFqdn.value = data.SERVER_FQDN
            sessionId.value = data.sessionId
            session.value = data
            const tdppTopic = "test"
            qrURI.value = "tdpp://" + serverFqdn.value + `/lp?cookie=${sessionId.value}&topic=${tdppTopic}`
        } catch (error) {
            console.error('Error fetching session data:', error)
        }
    }
}

export default async function ({ redirect }) {
    await fetchSession()
}

export { serverFqdn, sessionId, session, qrURI }
