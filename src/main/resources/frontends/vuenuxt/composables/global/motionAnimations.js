// useInViewAnimation.js
import { onMounted } from 'vue'
import { inView, animate } from 'motion'

export function useInViewAnimation(selector = "section", animationType = "fadeIn") {
    const animations = {
        fadeIn: { animationOptions: { opacity: [0, 1] }, transitionOptions: { easing: "ease-in", duration: 0.5 } },
        slideUp: { animationOptions: { transform: ["translateY(100px)", "translateY(0)"] }, transitionOptions: { easing: "ease-out", duration: 0.7 } },
        zoomIn: { animationOptions: { transform: ["scale(0.5)", "scale(1)"] }, transitionOptions: { easing: "ease-in-out", duration: 0.6 } },
    };

    const { animationOptions, transitionOptions } = animations[animationType] || animations.fadeIn;

    onMounted(() => {
        document.querySelectorAll(selector).forEach((element) => {
            if (element) {
                inView(element, () => {
                    animate(element, animationOptions, transitionOptions)
                });
            }
        })
    })
}

export function usePinningAnimation(selector = "li") {
    onMounted(() => {
        document.querySelectorAll(selector).forEach((element) => {
            if (element) {
                inView(element, () => {
                    animate(element, { opacity: [0, 1], y: [100, 0] }, { duration: 0.5 })
                }, { once: false });

                scroll(element, () => ({
                    offset: ['0px', '50%'],
                    enter: () => animate(element, { position: 'sticky', top: '10px', transition: { duration: 0.3 } }),
                    leave: () => animate(element, { position: 'relative', transition: { duration: 0.3 } })
                }))
            }
        });
    });
}
