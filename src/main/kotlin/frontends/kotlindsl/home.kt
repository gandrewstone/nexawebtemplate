package org.nexa.frontends.kotlindsl

import org.nexa.libnexakotlin.*
import io.ktor.http.*
import io.ktor.network.sockets.*
import io.ktor.server.application.*
import io.ktor.server.html.*
import io.ktor.server.sessions.*
import kotlinx.coroutines.delay
import kotlinx.html.*
import org.nexa.*
import java.net.URLEncoder
import java.text.SimpleDateFormat
import java.util.*
import kotlin.random.Random
import org.nexa.SharedResources.activeWallet
import org.nexa.urlEncode

val QR_SIZE = 300

fun createFaucetTx(session: AppSession): iTransaction?
{
    val wal: Wallet = activeWallet
    val chain = wal.chainSelector

    val wc = wal as CommonWallet
    val faucet = txFor(wal.chainSelector)

    // Add a payout to an address that the other wallet needs to fill in
    val payout = NexaTxOutput(wc.chainSelector, 1*KEX + Random.nextInt(0,10*KEX.toInt()), SatoshiScript(chain, SatoshiScript.Type.TEMPLATE, OP.C0, OP.TMPL_SCRIPT))
    faucet.add(payout)
    try
    {
        // fund that address but don't sign
        // EDIT: Actually don't fund now, fund when the tx comes back filled
        // wal.txCompleter(faucet, 0,TxCompletionFlags.FUND_NATIVE)
        // actually in this case we could just sign an open-ended payment (signature doesn't cover outputs) because we are giving away the money
        // but this is not a common use case so it is better to go thru the entire protocol.
    }
    catch (e: WalletNotEnoughBalanceException)
    {
        launch { session.log("faucet is drained") }
        return null
    }
    return faucet
}

fun DIV.welcome(session: AppSession)
{
    div(classes = "row tabGroup")
    {
        attributes["id"] = "WelcomeTab"
        div(classes = "inline")
        {
            h1("centered")
            {
                +tr("NEXA Wallet Integration Example and Testnet Faucet")
            }
            p("centered")
            {
                +tr("This example web app shows how a web site can be integrated with the NEXA blockchain and cryptocurrency.")
                +tr("  It uses the ")
                a(href = "https://spec.nexa.org/nexid/", target = "_blank") { +tr("Nexa Identity") }
                +tr(" and ")
                a(href = "https://spec.nexa.org/dpp/", target = "_blank") { +tr("Delegated Payment Protocol") }
                +tr(" to create simple interactions between a user's wallet and this app.")
            }
            h3("Instructions")
            p("centered")
            {
                +tr(
                    """This site presents a large number of QR codes as examples and for testing purposes, which makes it easy to accidentally scan the wrong QR.  
                             A normal web site would never present so many options on a single page, so they are greyed out until you want to try one."""
                   )
            }
            p("centered")
            {
                +tr(
                    """Hover your cursor over a QR code to increase the contrast so that your device can read it.  If your wallet and browser are on the same device,
                             or if you have connected your wallet using the QR code below, you can just click the QR code like a button."""
                   )
            }
            p("centered")
            {
                +tr("You MUST use ")
                a(href = "http://wallywallet.org/") { +tr("Wally Wallet") }
                +tr(
                    """ (the NEXA mobile wallet to try out the functionality in this app.
                        What if you don't want to install Wally but still want testnet funds?  Sorry, you can't (or anyway not easily)  -- think of this as a simple captcha.""".trimMargin()
                   )
            }

            h3("centered")
            {
                +tr("Troubleshooting")
            }
            p("centered")
            {
                +tr("Note when deploying this app yourself, the SERVER_FQDN (currently ${SERVER_FQDN}) has to be an address that your mobile device (running your wallet) can access.  So 'localhost' will not work.  Use a hard-coded local IP address and ensure that your phone's wifi is connected to the same network")
            }
        }

        div("inline spacer") {}
        div("inline")
        {
            h2("centered") {
                +tr("Connect Wallet")
            }
            var uri = "tdpp://" + SERVER_FQDN + "/lp?cookie=${session.sessionId?.id}&topic=${TDPP_TOPIC}"
            QrButton(uri)
            div()
            {
                +"Ping count:"
                div("inline longPollCount") { +"Not connected" }
            }
            div()
            {
                +"Connecting a wallet is optional; it allows you to click on all QR codes on this site as buttons rather than scanning them, even on different devices."
                +"If you are looking at this site from your wallet device, you can click on this QR as a button."
            }
        }
    }
}

fun DIV.signMessage(session: AppSession)
{
    div(classes = "row tabGroup")
    {
        attributes["id"] = "SignMessagesTab"
        h1("centered")
        {
            a(href = "https://spec.nexa.org/nexid/")
            {
                +tr("Sign Messages")
            }
        }
        div(classes = "inline")
        {
            div("row")
            {
                div("inline bold") { +"address: " }
                div("inline id_addr") { +"unspecified" }
            }
            div("row")
            {
                div("inline bold") { +"handle: " }
                div("inline id_hdl") { +"unspecified" }
            }
            div("row")
            {
                div("inline bold") { +"real name: " }
                div("inline id_realname") { +"unspecified" }
            }
            div("row")
            {
                div("inline bold") { +"postal address: " }
                div("inline id_postal") { +"unspecified" }
            }
            div("row")
            {
                div("inline bold") { +"billing address: " }
                div("inline id_billing") { +"unspecified" }
            }
            div("row")
            {
                div("inline bold") { +"birthday: " }
                div("inline id_dob") { +"unspecified" }
            }
            div("row")
            {
                div("inline bold") { +"avatar: " }
                div("inline id_ava") { +"unspecified" }
                img("", "", classes = "inline id_ava_img") {}
            }
            div("row")
            {
                div("inline inline bold") { +"phone: " }
                div("inline id_ph") { +"unspecified" }
            }
            div("row")
            {
                div("inline bold") { +"social media: " }
                div("inline id_sm") { +"unspecified" }
            }
            div("row")
            {
                div("inline inline bold id_sig_title") { +"signature: " }
                div("inline id_sig") { +"unspecified" }
            }
        }
        div("inline spacer") {}
        div(classes = "inline")
        {
            h2 {
                +tr("Sign Message")
            }
            session.textMessageToSign = tr("This is a test")
            div(classes = "inline")
            {
                textArea("20em", "100em", wrap = TextAreaWrap.hard) {
                    attributes["id"] = "whatToSign"
                    onInput = "signMessageChanged(this)"
                    +tr(session.textMessageToSign)
                }
            }
            div(classes = "inline signTextMessageQrButton") {
                // If the QR code depends on client page content (like this case), it is better to use javascript
                // after page load to grab that content and asynchronously send it to the server (as if the client changed the content).
                // Doing so means that the QR code will reflect contents that might have persisted across a page reload.
                // This code does this which is why you may see the QR code generated below quickly change.
                //
                // However, if you have an unchanging QR code that is pushed up as part of the page, you'd just do this:
                val uri = "nexid://" + SERVER_FQDN + "/_identity?op=sign&cookie=${session.sessionId!!.id}&proto=http&sign=" + URLEncoder.encode(session.textMessageToSign, "utf-8")
                QrButton(uri)  // commented out because this will be loaded from the page data after the page is up.  This fixes data persistent in the box through a page reload
            }
        }
        div("inline spacer") {}
        div(classes = "inline") {
            h2 {
                +tr("Sign Hex-encoded Binary Message")
            }
            session.binaryMessageToSign = "This is a binary message test".toByteArray()
            val b = session.binaryMessageToSign.toHex()
            div(classes = "inline")
            {
                textArea("20em", "100em", wrap = TextAreaWrap.hard) {
                    attributes["id"] = "whatToSignHex"
                    onInput = "signMessageChangedHex(this)"
                    +b
                }
            }

            div(classes = "inline signHexBinMessageQrButton") {
                // If the QR code depends on client page content (like this case), it is better to use javascript
                // after page load to grab that content and asynchronously send it to the server (as if the client changed the content).
                // Doing so means that the QR code will reflect contents that might have persisted across a page reload.
                // This code does this which is why you may see the QR code generated below quickly change.
                //
                // However, if you have an unchanging QR code that is pushed up as part of the page, you'd just do this:
                val uri = "nexid://" + SERVER_FQDN + "/_identity?op=sign&cookie=${session.sessionId!!.id}&proto=http&signhex=" + b
                QrButton(uri)
            }
        }

    }
}

fun DIV.walletDetails(session: AppSession)
{
    div(classes = "row tabGroup")
    {
        attributes["id"] = "WalletInternalsTab"
        h1("centered")
        {
            +tr("Wallet Internals")
        }
        div(classes = "inline")
        {
            val wal: Wallet = activeWallet
            val chain = wal.chainSelector
            val wc = wal as CommonWallet
            h3 {
                +tr("P2P Peers")
            }
            small {
                val cxns = wc.blockchain.net.p2pCnxns
                p { +tr("${cxns.size} peers") }
                for (c in cxns)
                {
                    p { +"${c.logName}: Latency: ${c.aveLatency}ms Sent: ${c.bytesSent/1024}KB  Received: ${c.bytesReceived/1024}KB"}
                }
            }
            h3 {
                +tr("Blockchain")
            }
            small {
                val bc = wc.blockchain
                val simpleDateFormat = SimpleDateFormat("dd MMMM yyyy, HH:mm:ss", Locale.ENGLISH)
                val ts = simpleDateFormat.format((bc.nearTip?.time ?: 0L) * 1000L)
                p { + "Tip: ${bc.nearTip?.height}:${bc.nearTip?.hash} found at: ${ts} (epoch: ${bc.nearTip?.time})"}
            }


            h3 {
                +tr("Wallet Dump")
            }
            small {
                small {
                    unsafe {
                        +wc.debugDump().replace("\n", "<br/>\n")
                    }
                }
            }
        }
    }
}

fun DIV.faucet(session: AppSession)
{
    div(classes = "row tabGroup")
    {
        attributes["id"] = "FaucetTab"
        h1("centered")
        {
            +tr("Faucet (receive and send some coins)")
        }

        div(classes = "inline")
        {
            h2 {
                +tr("Get Testnet Coins")
            }

            p { +("Faucet currently has " + displayCurrency(activeWallet.balance) + "\nat block " + activeWallet.syncedHeight.toString() + " of " + activeWallet.blockchain.curHeight +".\n" + ((activeWallet as CommonWallet).chainstate?.chain?.net?.size ?: "") + " peers") }
            val faucetTx = createFaucetTx(session)
            if (faucetTx != null)
            {
                val chain = chainToURI[activeWallet.chainSelector]
                val hexTx = faucetTx?.toHex()
                session.faucetTx = faucetTx
                // see https://spec.nexa.org/dpp/
                // val uri = "tdpp://" + SERVER_FQDN + "/tx?chain=${chain}&cookie=${session.sessionId!!.id}&inamt=${faucetTx.inputTotal}&topic=${TDPP_TOPIC}&tx=${hexTx}"
                // flags = nofund and nopost.  This is a bit of a degenerate case, we are just asking for an output to send $ to... normally the server might be asking for a token or funding input
                // we don't want the client to post the transaction, because its not complete.  When we get it back we need to fund it.
                val uri = "tdpp://" + SERVER_FQDN + "/tx?chain=${chain}&cookie=${session.sessionId!!.id}&flags=3&inamt=${1 * NEX}&topic=${TDPP_TOPIC}&tx=${hexTx}"
                launch { delay(1000); session.log("""Faucet request signed: '${uri}'""") }
                val sig = activeWallet.signMessage(uri, SERVER_IDENTITY_ADDRESS)
                val signedUri = uri + "&sig=" + URLEncoder.encode(sig, "UTF-8")
                QrButton(uri, QR_SIZE)
            }
            else p { +tr("  OFFLINE  ") }
        }
        div(classes = "inline")
        {
            h2 {
                +tr("Return funds")
            }
            QrButton(RECV_ADDR)
            p { +RECV_ADDR }
        }
        div(classes = "inline")
        {
            h2 {
                +tr("Return " + displayCurrency(123456789L) + " via BIP21")
            }
            // since the RECV_ADDR already contains the chain name and a colon, the uri schema is already there
            QrButton(RECV_ADDR + "?amount=1234567.89&label=faucet")
        }
    }
}

fun DIV.identity(session: AppSession)
{
    div(classes = "row tabGroup")
    {
        attributes["id"] = "IdentityTab"
        h1("centered")
        {
            a(href = "https://spec.nexa.org/nexid/")
            {
                +tr("Identity and Login Functions")
            }
        }

        div("inline spacer") {}
        div(classes = "inline") {
            h2 {
                +tr("Information / Registration / Login")
            }
            p {
                +tr("Requiring a username, and optionally a real name")
            }
            val uri = "nexid://" + SERVER_FQDN + "/_identity?chal=${session.identityChallenge}&op=reg&cookie=${session.sessionId!!.id}&hdl=m&proto=http&realname=o"
            QrButton(uri)
        }
        div("inline spacer") {}
        div(classes = "inline") {
            h2 {
                +tr("Information / Registration / Login")
            }
            p {
                +tr("During a purchasing process a site might offer")
            }
            p {
                +tr("a QR code to easily fill out fields such as postal address")
            }
            // Now real name and postal address are mandatory
            val uri = "nexid://" + SERVER_FQDN + "/_identity?chal=${session.identityChallenge}&op=reg&cookie=${session.sessionId!!.id}&proto=http&realname=m&postal=m"
            QrButton(uri)
        }
        div("inline spacer") {}
        div(classes = "inline")
        {
            h2()
            {
                +tr("Login only")
            }
            p {
                +tr("You must have previously registered")
            }
            val uri = "nexid://" + SERVER_FQDN + "/_identity?chal=${session.identityChallenge}&op=login&cookie=${session.sessionId!!.id}&proto=http"
            QrButton(uri)
        }
    }
}

fun DIV.tdpp(session: AppSession)
{
    div(classes = "row tabGroup")
    {
        attributes["id"] = "DelegatedPaymentTab"

        h1("centered")
            {
                a(href = "https://spec.nexa.org/dpp/")
                {
                    +tr("Delegated Payment Protocol")
                }
            }

        div("row")
        {
            div("inline")
            {
                val hdr = tr("Payment registration example")
                h2("centered")
                {
                    +hdr
                }
                launch { delay(1000); session.log("""server identity address: '${SERVER_IDENTITY_ADDRESS!!.toString()}'""") }
                val urlAddr = SERVER_IDENTITY_ADDRESS!!.urlEncode()
                val desc = URLEncoder.encode("1000 tNEX example payment\u2602", "utf-8")
                var uri = "tdpp://" + SERVER_FQDN + "/reg?addr=${urlAddr}&cookie=${session.sessionId?.id}&descper=${desc}&maxper=100000&topic=${TDPP_TOPIC}"
                val uriToSign = uri  // have to make a copy to log this asynchronously because uri is modified
                launch { delay(1000); session.log("""'$hdr' example signed '${uriToSign}'""") }
                val sig = activeWallet.signMessage(uri, SERVER_IDENTITY_ADDRESS)
                uri = uri + "&sig=" + URLEncoder.encode(sig, "UTF-8")
                QrButton(uri)
            }
            div("inline spacer") {}
            div("inline")
            {
                val qty = 100000L
                val hdr = tr("Payment request for " + displayCurrency(qty))
                h2("centered")
                {
                    +hdr
                }
                var addr = activeWallet.getNewAddress()
                var uri = "tdpp://" + SERVER_FQDN + "/sendto?addr0=${addr.urlEncode()}&amt0=$qty&chain=nexatest&cookie=${session.sessionId?.id}&topic=${TDPP_TOPIC}"
                val uriToSign = uri  // have to make a copy because uri is modified
                launch { delay(1000); session.log("""'$hdr' example signed '${uriToSign}'""") }
                val sig = activeWallet.signMessage(uri, SERVER_IDENTITY_ADDRESS)
                uri = uri + "&sig=" + sig.urlEncode()
                QrButton(uri)
            }
            div("inline spacer") {}
            div("inline")
            {
                val qty = 200000L
                val hdr = tr("Payment request for " + displayCurrency(qty) + " (exceeds threshold)")
                h2("centered")
                {
                    +hdr
                }
                var addr = activeWallet.getNewAddress()
                var uri = "tdpp://" + SERVER_FQDN + "/sendto?addr0=${addr.urlEncode()}&amt0=$qty&chain=nexatest&cookie=${session.sessionId?.id}&topic=${TDPP_TOPIC}"
                val uriToSign = uri  // have to make a copy because uri is modified
                // launch { delay(1000); session.log("""'$hdr' signed '${uriToSign}'""") }
                val sig = activeWallet.signMessage(uri, SERVER_IDENTITY_ADDRESS)
                uri = uri + "&sig=" + sig.urlEncode()
                QrButton(uri)
            }
            div("inline spacer") {}
        }
    }
}


fun DIV.reverseQR(session: AppSession)
{
    div(classes = "row tabGroup")
    {
        attributes["id"] = "ReverseQrTab"
        div(classes = "inline")
        {
            h2 {
                +tr("Reverse QR (request data from the mobile device)")
            }
            div(classes = "inline reverseQRcontents")
            {
                /*
                textArea("10em", "80em", wrap = TextAreaWrap.hard) {
                    attributes["id"] = "providedData"
                    onInput = "providedDataChanged(this)"
                    +""
                }

                 */
            }
            div(classes = "inline")
            {
                h2 {
                    +tr("Clipboard")
                }
                div(classes = "reverseQR") {
                    val uri = "tdpp://" + SERVER_FQDN + "/share?cookie=${session.sessionId?.id}&topic=${TDPP_TOPIC}"
                    QrButton(uri, 3 * QR_SIZE / 4)
                }
            }
            div(classes = "inline")
            {
                h2 {
                    +tr("Nexa Address")
                }
                div(classes = "reverseQR") {
                    val uri = "tdpp://" + SERVER_FQDN + "/share?cookie=${session.sessionId?.id}&info=address&topic=${TDPP_TOPIC}"
                    QrButton(uri, 3 * QR_SIZE / 4)
                }
            }
        }
    }
}

suspend fun homePage(call: ApplicationCall)
{
    var (id, session) = sessionNewPage(call)
    call.respondHtml(HttpStatusCode.OK)
    {
        appHeader()
        appBody(session,"", listOf("home.js"))
        {
            div("tabHorizMenu") {
                a(null, null, classes = "tabItem") {
                    onClick = "focusTab('tabGroup','WelcomeTab')"
                    onMouseOver = "highlightMenuItem(this)"
                    onMouseOut = "unhighlightMenuItem(this)"
                    +"Welcome"
                }
                +" "
                a(null, null,classes = "tabItem") {
                    onClick = "focusTab('tabGroup','FaucetTab')"
                    onMouseOver = "highlightMenuItem(this)"
                    onMouseOut = "unhighlightMenuItem(this)"
                    +"Faucet"
                }
                +" "
                a(null, null,classes = "tabItem") {
                    onClick = "focusTab('tabGroup','SignMessagesTab')"
                    onMouseOver = "highlightMenuItem(this)"
                    onMouseOut = "unhighlightMenuItem(this)"
                    +"Sign Messages"
                }
                +" "
                a(null, null,classes = "tabItem") {
                    onClick = "focusTab('tabGroup','IdentityTab')"
                    onMouseOver = "highlightMenuItem(this)"
                    onMouseOut = "unhighlightMenuItem(this)"
                    +"Identity"
                }
                +" "
                a(null, null,classes = "tabItem") {
                    onClick = "focusTab('tabGroup','ReverseQrTab')"
                    onMouseOver = "highlightMenuItem(this)"
                    onMouseOut = "unhighlightMenuItem(this)"
                    +"Reverse QR code"
                }
                +" "
                a(null, null,classes = "tabItem") {
                    onClick = "focusTab('tabGroup','DelegatedPaymentTab')"
                    onMouseOver = "highlightMenuItem(this)"
                    onMouseOut = "unhighlightMenuItem(this)"
                    +"Delegated Payment"
                }
                +" "
                a(null, null,classes = "tabItem") {
                    onClick = "focusTab('tabGroup','WalletInternalsTab')"
                    onMouseOver = "highlightMenuItem(this)"
                    onMouseOut = "unhighlightMenuItem(this)"
                    +"Wallet Internals"
                }

            }
            div("medhr") {}

            welcome(session)
            faucet(session)
            signMessage(session)
            identity(session)
            tdpp(session)
            reverseQR(session)
            walletDetails(session)

            div("medhr") {}
            h1("centered")
            {
                +tr("For developers")
            }
            h2("centered")
            {
                "The contents of the last QR code under the cursor"
            }
            div(classes = "info")
            {
                +tr("")
            }

            h2("centered")
            {
                +tr("Logging")
            }
            p(classes = "log")
            {

            }
        }

    }

}

