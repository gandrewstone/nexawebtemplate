package org.nexa.frontends.kotlindsl

import org.nexa.libnexakotlin.CurrencyDecimal
import org.nexa.libnexakotlin.chainToCurrencyCode
import kotlinx.html.*
import org.nexa.libnexakotlin.NexaFormat
import com.ionspin.kotlin.bignum.decimal.BigDecimal
import org.nexa.*
import org.nexa.SharedResources.activeWallet
import org.nexa.libnexakotlin.toLong

// Add needed fields to the script tag
@HtmlTagMarker
fun mscript(consumer: TagConsumer<*>, type : String? = null, src : String? = null, integrity : String? = null, crossorigin: String? = null, defer: String? = null, referrerPolicy: String? = null) : Unit =
    SCRIPT(attributesMapOf("type", type,"src", src, "integrity", integrity, "crossorgin", crossorigin, "defer", defer, "referrerPolicy", referrerPolicy), consumer).visit({})

val defaultCurrencyUnitFormat = NexaFormat

fun defaultToFinestCurrencyUnit(x:String): Long = defaultToFinestCurrencyUnit(CurrencyDecimal(x))

// 100 sats in a Nexa
fun defaultToFinestCurrencyUnit(x: BigDecimal) = (x*BigDecimal.fromInt(100)).toLong()
fun defaultToFinestCurrencyUnit(x:Long) = (x*100).toLong()
fun finestToDefaultCurrencyUnit(x:Long): BigDecimal
{
    val ret = CurrencyDecimal(x) / CurrencyDecimal(100)
    return ret
}


fun HEAD.ogmeta(property : String, content : String,  block: META.() -> Unit = {}) : Unit
{
    meta()
    {
        attributes["property"] = property
        attributes["content"] = content
        block()
    }
}

fun BODY.preScripts(js:List<String>? = null)
{
    if (js != null) for (j in js)
        mscript(consumer, type = "application/javascript","/static/$j")

    // TODO add any script you need to be at the top of the body

    // Example, add bootstrap
    // mscript(consumer, type = "application/javascript", src = "/static/bootstrap.bundle.min.js")
    // mscript(consumer, type = "application/javascript", src = "/static/vue.esm-browser.js")
}

fun BODY.postScripts(js:List<String>? = null)
{
    if (js != null) for (j in js)
        mscript(consumer, type = "application/javascript","/static/$j")

    // TODO add any script you need to be at the bottom of the body
}


fun DIV.QrButton(scanClickUri: String, qrSize: Int = QR_SIZE )
{

    form()
    {
        button(classes = "dim QRbutton", type = ButtonType.button)
        {
            attributes["onclick"] = "buttonClicked('$scanClickUri');"
            // Dims the prior QR code when a new one is entered
            attributes["onmouseover"] = "showInfo('$scanClickUri'); dimAll('QRbutton'); brighten(this);"
            // to dim on mouse out: attributes["onmouseout"] = "showInfo('$scanClickUri'); dim(this);"
            unsafe() { +createQrSvg("QR", scanClickUri, qrSize) }
        }
    }
}

fun HTML.appBody(session: AppSession?, title: String? = null, js:List<String>? = null, bodyBlock: DIV.() -> Unit)
{
    body()
    {
        preScripts(js)
        div()
        {
            bodyBlock()
        }
        postScripts()
    }
}

fun HTML.appHeader(subTitle: String? = null, css:List<String>? = null)
{
    head {
        meta(charset ="utf-8")
        meta(name = "viewport", content="width=device-width, initial-scale=1")
        link(rel="icon", type="image/png", href="/static/icon.png??v=$REFRESH_VERSION")
        link("/static/styles.css?v=$REFRESH_VERSION", rel="stylesheet")
        if (css != null) for (c in css)
        {
            link("/static/$c?v=$REFRESH_VERSION", rel = "stylesheet")
        }
        title(APP_TITLE +  if (subTitle!=null) ":" + subTitle else "")

        ogmeta("og:title", APP_TITLE)
        ogmeta("og:description",APP_DESCRIPTION)
        ogmeta("og:site_name", APP_TITLE)
        ogmeta("og:url",SERVER_FQDN)
        ogmeta("og:image", SERVER_FQDN + "/" + SITE_FLAG_IMG)
        ogmeta("og:type", "website")
    }
}
