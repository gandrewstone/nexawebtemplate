package org.nexa.api.controllers

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import org.nexa.*
import org.nexa.api.models.SignMessageData
import java.net.URLEncoder
import org.nexa.libnexakotlin.toHex
import org.nexa.libnexakotlin.fromHex
import java.util.logging.Logger

/**
 * Controller for handling sign message-related endpoints.
 */
private val LogIt = Logger.getLogger("nexa.session")

/**
 * Retrieves sign message-related data, including QR URIs for signing messages.
 * Responds with a [SignMessageData] object containing the relevant information.
 *
 * @param call The ApplicationCall instance.
 */
suspend fun getSignMessageData(call: ApplicationCall) {
    val session = sessionFromCookie(call) ?: return

    val textMessageToSign = tr("This is a test")
    val binaryMessageToSign = "This is a binary message test".toByteArray().toHex()

    val signTextMessageQrUri = "nexid://${SERVER_FQDN}/_identity?op=sign&cookie=${session.sessionId!!.id}&proto=http&sign=${URLEncoder.encode(textMessageToSign, "utf-8")}"
    val signHexBinMessageQrUri = "nexid://${SERVER_FQDN}/_identity?op=sign&cookie=${session.sessionId!!.id}&proto=http&signhex=${binaryMessageToSign}"

    val data = SignMessageData(
        address = "unspecified",
        handle = "unspecified",
        realName = "unspecified",
        postalAddress = "unspecified",
        billingAddress = "unspecified",
        birthday = "unspecified",
        avatar = "unspecified",
        phone = "unspecified",
        socialMedia = "unspecified",
        signature = "unspecified",
        textMessageToSign = textMessageToSign,
        binaryMessageToSign = binaryMessageToSign,
        signTextMessageQrUri = signTextMessageQrUri,
        signHexBinMessageQrUri = signHexBinMessageQrUri
    )
    call.respond(data)
}

/**
 * Handles requests to change the sign message, generating the appropriate QR code URIs.
 *
 * @param call The ApplicationCall instance.
 */
suspend fun changeSignMessage(call: ApplicationCall) {
    val cookie = call.parameters["cookie"]
    if (cookie == null) {
        LogIt.info("bad request, no cookie parameter")
        call.respondText("Invalid request", status = HttpStatusCode.BadRequest)
    } else {
        val session = sessionFromCookie(cookie)
        if (session == null) {
            call.respondText("Unknown session", status = HttpStatusCode.NotFound)
        } else {
            val msg = call.parameters["msg"]
            val hexmsg = call.parameters["msghex"]
            if (msg != null) {
                session.textMessageToSign = msg
                val uri = "nexid://${SERVER_FQDN}/_identity?op=sign&cookie=${session.sessionId!!.id}&sign=" + URLEncoder.encode(session.textMessageToSign, "utf-8")
                call.respondText(uri, status = HttpStatusCode.OK)
            } else if (hexmsg != null) {
                try {
                    session.binaryMessageToSign = hexmsg.fromHex()
                    val uri = "nexid://${SERVER_FQDN}/_identity?op=sign&cookie=${session.sessionId!!.id}&signhex=" + hexmsg
                    call.respondText(uri, status = HttpStatusCode.OK)
                } catch (e: Exception) {
                    call.respondText("Invalid hex encoding", status = HttpStatusCode.BadRequest)
                }
            } else {
                call.respondText("changeSignMessage: missing parameter", status = HttpStatusCode.BadRequest)
            }
        }
    }
}
