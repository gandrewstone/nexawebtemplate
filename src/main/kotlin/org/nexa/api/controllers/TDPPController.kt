// src/main/kotlin/org/nexa/api/controllers/TdppController.kt
package org.nexa.api.controllers

import io.ktor.server.application.*
import io.ktor.server.response.*
import kotlinx.coroutines.delay
import org.nexa.*
import org.nexa.SharedResources.activeWallet
import org.nexa.api.models.QrCodeData
import org.nexa.api.models.TDPPData
import org.nexa.libnexakotlin.launch
import java.net.URLEncoder

/**
 * Controller for handling Delegated Payment Protocol (TDPP) related endpoints.
 */

/**
 * Retrieves TDPP-related data, including registration and payment request URIs.
 * Responds with a [TDPPData] object containing the relevant information.
 *
 * @param call The ApplicationCall instance.
 * @param session The current application session.
 */
suspend fun getTdppQRData(call: ApplicationCall) {
    val session = sessionFromCookie(call) ?: return

    val qrCodeDataList = mutableListOf<QrCodeData>()

    val hdr = tr("Payment registration example")
    launch { delay(1000); session.log("""server identity address: '${SERVER_IDENTITY_ADDRESS!!.toString()}'""") }
    val urlAddr = SERVER_IDENTITY_ADDRESS!!.urlEncode()
    val desc = URLEncoder.encode("1000 tNEX example payment\u2602", "utf-8")
    var uri = "tdpp://${SERVER_FQDN}/reg?addr=$urlAddr&cookie=${session.sessionId?.id}&descper=$desc&maxper=100000&topic=$TDPP_TOPIC"
    val uriToSign = uri
    launch { delay(1000); session.log("""'$hdr' example signed '$uriToSign'""") }
    val sig = activeWallet.signMessage(uri, SERVER_IDENTITY_ADDRESS)
    uri += "&sig=" + URLEncoder.encode(sig, "UTF-8")
    val regUri = uri

    qrCodeDataList.add(
        QrCodeData(
            title = "Payment registration example",
            subtitle = "",
            qrURI = regUri
        )
    )

    val qty1 = 100000L
    val hdr1 = tr("Payment request for " + displayCurrency(qty1))
    var addr1 = activeWallet.getNewAddress()
    var uri1 = "tdpp://${SERVER_FQDN}/sendto?addr0=${addr1.urlEncode()}&amt0=$qty1&chain=nexatest&cookie=${session.sessionId?.id}&topic=$TDPP_TOPIC"
    val sig1 = activeWallet.signMessage(uri1, SERVER_IDENTITY_ADDRESS)
    uri1 += "&sig=" + sig1.urlEncode()
    val sendUri1 = uri1

    qrCodeDataList.add(
        QrCodeData(
            title = "Payment request for " + displayCurrency(qty1),
            subtitle = "",
            qrURI = sendUri1
        )
    )

    val qty2 = 200000L
    val hdr2 = tr("Payment request for " + displayCurrency(qty2) + " (exceeds threshold)")
    var addr2 = activeWallet.getNewAddress()
    var uri2 = "tdpp://${SERVER_FQDN}/sendto?addr0=${addr2.urlEncode()}&amt0=$qty2&chain=nexatest&cookie=${session.sessionId?.id}&topic=$TDPP_TOPIC"
    val sig2 = activeWallet.signMessage(uri2, SERVER_IDENTITY_ADDRESS)
    uri2 += "&sig=" + sig2.urlEncode()
    val sendUri2 = uri2

    qrCodeDataList.add(
        QrCodeData(
            title = "Payment request for " + displayCurrency(qty2) + " (exceeds threshold)",
            subtitle = "",
            qrURI = sendUri2
        )
    )

    call.respond(qrCodeDataList)
}
