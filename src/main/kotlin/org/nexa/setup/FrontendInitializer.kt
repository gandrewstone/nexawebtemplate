// src/main/kotlin/org/nexa/setup/FrontendInitializer.kt
package org.nexa.setup

import kotlinx.serialization.*
import kotlinx.serialization.json.*
import org.nexa.CONFIG_FILE
import org.nexa.api.models.AppConfig
import org.nexa.api.models.ClientConfig
import org.nexa.api.models.ServerConfig
import java.io.File
import java.io.IOException
import java.nio.file.Paths
import java.util.Scanner

const val GREEN = "\u001B[32m"
const val YELLOW = "\u001B[33m"
const val RESET = "\u001B[0m"
const val RED = "\u001B[31m"

// Function to initialize the frontend project
fun initializeFrontend(projectName: String, framework: String, projectDir: String) {
    val frontendDir = File("$projectDir/$projectName")

    // Check if a frontend project already exists
    if (frontendDir.exists()) {
        print("$RED !! A frontend project already exists at ${frontendDir.absolutePath} !! $YELLOW Do you want to delete it? $GREEN (yes/no):$RESET")
        val scanner = Scanner(System.`in`)
        val response = scanner.nextLine()

        // Delete existing project if user confirms
        if (response.lowercase() == "yes") {
            frontendDir.deleteRecursively()
            println("Existing frontend project deleted.")
        } else {
            println("Operation cancelled.")
            return
        }
    }

    // Define CLI commands based on the chosen framework
    val commands = when (framework.lowercase()) {
        "reactnext" -> listOf("npx", "create-next-app@latest", projectName, "--use-npm", "--typescript", "--eslint", "--no-tailwind", "--src-dir", "--app", "--import-alias", "@/*")
        "vuenuxt" -> listOf("npx", "nuxi@latest", "init", projectName, "--package-manager=npm", "--git-init=false", "--no-install=false", "--prefer-offline=false")
        "nuxt-faucet" -> listOf("git", "clone", "https://gitlab.com/myendy/nexa-webtemplate-nuxt-boilerplate.git", projectName)
        else -> throw IllegalArgumentException("Unsupported framework: $framework")
    }

    // Check if required CLI tools are installed
    if (!isCommandAvailable(commands[0])) {
            println("""
            The required CLI tool '${commands[0]}' is not installed. Please install it and try again.
            You can install it using the following commands:
            
            React (Next.js): npm install -g create-next-app
            Vue (Nuxt.js): npm install -g nuxi
            Nuxt-Faucet: Ensure git is installed on your system
            
            Make sure you have Node.js and npm installed. You can download them from https://nodejs.org/
        """.trimIndent())
        return
    }

    // Try to execute the CLI command to initialize the project
    try {
        val processBuilder = ProcessBuilder(commands)
        processBuilder.directory(File(projectDir))
        processBuilder.inheritIO()
        val process = processBuilder.start()
        process.waitFor()

        if (process.exitValue() == 0) {
            println("$framework project $projectName initialized successfully.")

            // Additional configuration for React
            if (framework.lowercase() == "react") {
                addNextConfig(projectName, projectDir)
            }

            // Create .env file for Nuxt faucet boilerplate
            if (framework.lowercase() == "nuxt-faucet") {
                createEnvFile(projectName, projectDir)
            }

            // Install project dependencies
            if (!installDependencies(projectName, projectDir)) {
                frontendDir.deleteRecursively()
                println("$RED !! Failed to install dependencies. Frontend project $projectName deleted. !!$RESET")
                return
            }

            // Build the project
            buildProject(projectName, projectDir, framework)

            // Update application configuration
            updateAppConfig(projectName, framework, projectDir)
        } else {
            frontendDir.deleteRecursively()
            println("$RED !! Failed to initialize $framework project $projectName. Frontend project deleted. !!$RESET")
        }
    } catch (e: IOException) {
        frontendDir.deleteRecursively()
        println("$RED !! Error: ${e.message}. Frontend project deleted. !!$RESET")
    }
}

// Function to check if a command is available on the system
fun isCommandAvailable(command: String): Boolean {
    return try {
        val process = ProcessBuilder(command, "--version")
            .redirectErrorStream(true)
            .start()
        process.waitFor() == 0
    } catch (e: IOException) {
        false
    }
}

// Function to install npm dependencies for the project
fun installDependencies(projectName: String, projectDir: String): Boolean {
    println("Installing dependencies...")
    val processBuilder = ProcessBuilder("npm", "install")
    processBuilder.directory(File("$projectDir/$projectName"))
    processBuilder.inheritIO()

    val process = processBuilder.start()
    process.waitFor()

    return process.exitValue() == 0
}

// Function to build the frontend project
fun buildProject(projectName: String, projectDir: String, framework: String) {
    println("Building the project...")

    // Define build commands based on the chosen framework
    val buildCommands = when (framework.lowercase()) {
        "reactnext" -> listOf(
            listOf("npm", "run", "build")
        )
        "vuenuxt" -> listOf(
            listOf("npm", "run", "build"),
            listOf("npm", "run", "generate")
        )
        "nuxt-faucet" -> listOf(
            listOf("npm", "run", "build"),
            listOf("npm", "run", "generate")
        )
        else -> throw IllegalArgumentException("Unsupported framework: $framework")
    }

    // Execute build commands
    for (command in buildCommands) {
        val processBuilder = ProcessBuilder(command)
        processBuilder.directory(File("$projectDir/$projectName"))
        processBuilder.inheritIO()

        val process = processBuilder.start()
        process.waitFor()

        if (process.exitValue() != 0) {
            println("Failed to execute command: ${command.joinToString(" ")}")
            return
        }
    }

    println("Project built successfully.")
}

// Function to add a custom Next.js configuration file
fun addNextConfig(projectName: String, projectDir: String) {
    val nextConfigContent = """
        /**
        * @type {import('next').NextConfig}
        */
        const nextConfig = {
            output: 'export',
        }
        export default nextConfig
    """.trimIndent()

    val nextConfigFile = File("$projectDir/$projectName/next.config.mjs")
    nextConfigFile.writeText(nextConfigContent)
    println("$GREEN next.config.mjs file created with custom configuration for $projectName.$RESET")
}

// Function to create .env file for Nuxt faucet boilerplate
fun createEnvFile(projectName: String, projectDir: String) {
    val envContent = """
        BASE_URL=http://localhost:3000
        API_BASE_URL=localhost:7997
    """.trimIndent()

    val envFile = File("$projectDir/$projectName/.env")
    envFile.writeText(envContent)
    println("$GREEN .env file created with BASE_URL and API_BASE_URL for $projectName.$RESET")
}

// Function to update the application configuration file
@OptIn(ExperimentalSerializationApi::class)
fun updateAppConfig(projectName: String, framework: String, projectDir: String) {
    val configFile = File(CONFIG_FILE)
    if (!configFile.exists()) {
        println("$RED !! app.cfg not found. Please ensure it exists at the root of the project. !!$RESET")
        val frontendDir = File("$projectDir/$projectName")
        frontendDir.deleteRecursively()
        println("$RED !! Frontend project $projectName deleted due to missing app.cfg. !!$RESET")
        return
    }

    val configText = configFile.readText(Charsets.UTF_8)
    val appConfig: AppConfig = try {
        Json.decodeFromString(AppConfig.serializer(), configText)
    } catch (e: MissingFieldException) {
        // Try to deserialize the server section separately if client section is missing
        val serverConfig: ServerConfig = try {
            val jsonObject = Json.decodeFromString(JsonObject.serializer(), configText)
            Json.decodeFromJsonElement(ServerConfig.serializer(), jsonObject["server"]!!)
        } catch (e: Exception) {
            println("$RED !! 'server' section missing from app.cfg. Please add it manually. !!$RESET")
            return
        }

        // Prompt user to add the missing client section
        print("$RED !! 'client' section missing from app.cfg. !! $YELLOW Do you want to add it? $GREEN (yes/no):$RESET")
        val scanner = Scanner(System.`in`)
        val response = scanner.nextLine()
        if (response.lowercase() == "yes") {
            AppConfig(
                server = serverConfig,
                client = mapOf(projectName to ClientConfig(
                    location = "src/main/resources/frontends/$projectName",
                    framework = framework,
                    routeprefix = "/"
                )),
                activeClients = setOf(projectName)
            ).also {
                val json = Json { prettyPrint = true; prettyPrintIndent = "  " }
                configFile.writeText(json.encodeToString(AppConfig.serializer(), it))
                println("$GREEN app.cfg updated with location and framework for $projectName.$RESET")
            }
            return
        } else {
            println("Operation cancelled.")
            return
        }
    }

    // Make a mutable copy of the client map
    val updatedClient = appConfig.client.toMutableMap()

    // Ensure the client section exists and update it
    // This prevents potential null pointer exception further down
    val clientConfig = updatedClient.getOrPut(projectName) {
        ClientConfig(
            location = "src/main/resources/frontends/$projectName",
            framework = framework,
            routeprefix = "/"
        )
    }

    clientConfig.location = "src/main/resources/frontends/$projectName"
    clientConfig.framework = framework

    // Update the map with the modified clientConfig
    updatedClient[projectName] = clientConfig

    // Ensure projectName is in activeClients
    val updatedActiveClients = appConfig.activeClients.toMutableSet()
    updatedActiveClients.add(projectName)

    // Create a new AppConfig with updated values
    val updatedAppConfig = AppConfig(
        server = appConfig.server,
        client = updatedClient,
        activeClients = updatedActiveClients
    )

    // Pretty print configuration
    val json = Json {
        prettyPrint = true
        prettyPrintIndent = "  "
    }

    configFile.writeText(json.encodeToString(AppConfig.serializer(), updatedAppConfig))
    println("$GREEN app.cfg updated with location and framework for $projectName.$RESET")
}

// Main function to execute the setup process
fun main() {
    val scanner = Scanner(System.`in`)

    // Display Node.js and npm versions
    val nodeVersionProcess = ProcessBuilder("node", "--version").start()
    nodeVersionProcess.waitFor()
    println("Node.js version: ${nodeVersionProcess.inputStream.bufferedReader().readText()}")

    val npmVersionProcess = ProcessBuilder("npm", "-v").start()
    npmVersionProcess.waitFor()
    println("npm version: ${npmVersionProcess.inputStream.bufferedReader().readText()}")

    // Prompt user for project name
    print("${GREEN}Enter the project name:${RESET} ")
    val projectName = scanner.nextLine()

    // List available frameworks
    val frameworks = listOf("React (Next.js)", "Vue (Nuxt.js)", "Vue (Nuxt.js) - Faucet Boilerplate")

    frameworks.forEachIndexed { index, framework -> println("${index + 1}. $framework") }

    // Prompt user to choose a framework
    print("${GREEN}Choose a framework: $RESET")
    val frameworkChoice = scanner.nextLine().toIntOrNull()
    if (frameworkChoice == null || frameworkChoice !in 1..frameworks.size) {
        println("$RED Invalid choice.$RESET")
        return
    }

    val framework = when (frameworkChoice) {
        1 -> "reactnext"
        2 -> "vuenuxt"
        3 -> "nuxt-faucet"
        else -> throw IllegalArgumentException("Invalid choice")
    }

    // Get the absolute path for the project directory
    val projectDir = Paths.get("src/main/resources/frontends").toAbsolutePath().toString()

    // Ensure the frontends directory exists
    File(projectDir).mkdirs()

    // Initialize the frontend project
    initializeFrontend(projectName, framework, projectDir)
}
