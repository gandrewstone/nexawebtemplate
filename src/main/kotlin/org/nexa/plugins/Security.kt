package org.nexa.plugins

import io.ktor.server.sessions.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.request.*
import io.ktor.server.routing.*
import org.nexa.*

fun Application.configureSecurity() {
    install(Sessions) {
        cookie<AppSessionId>("appSess") {
            cookie.path = "/"
            cookie.extensions["SameSite"] = "lax"
        }
    }

    routing {
        get("/session/increment") {
                val session = call.sessions.get<AppSessionId>() ?: AppSession()
                //call.sessions.set(session.copy(count = session.count + 1))
                //call.respondText("Counter is ${session.count}. Refresh to increment.")
            }
    }
}
